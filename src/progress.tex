\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{float}
\usepackage[tableposition=top]{caption}
\usepackage{pgfgantt}

\floatstyle{plaintop}
\restylefloat{table}

\linespread{1.15}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering

	{\huge University of Waterloo \\}
	{\Large Faculty of Engineering \\}
	{\Large Department of Electrical and Computer Engineering}

	\vspace{1.2in}

	{\Huge The Copper Chef}

	\vspace{1.2in}

	{\Large Group 2018.050 \\}

	\vspace{1.2in}

	{\Large Project Consultant \\}
	{\large Professor Christopher Nielsen}

	\vspace{1.2in}

	{\Large Prepared By \\}

	\begin{table}[h!]
	\centering
	\begin{tabular}{l l r}
		{\large Eric Bender} & {\large etbender@uwaterloo.ca} & {\large 20534384} \\
		{\large Shiva Ehtezazi} & {\large sehtezaz@uwaterloo.ca} & {\large 20529687} \\
		{\large Taylor Laekeman} & {\large tjlaekem@uwaterloo.ca} & {\large 20484137} \\
		{\large Christian Petri} & {\large capetri@uwaterloo.ca} & {\large 20484960} \\
		{\large Brian Truong} & {\large b3truong@uwaterloo.ca} & {\large 20528564}
	\end{tabular}
	\end{table}
\end{titlepage}
\newpage

\tableofcontents
\newpage
\listoffigures
\listoftables
\newpage

\pagenumbering{arabic}

\section{Project Overview}

This section outlines the aim of the project and presents the project’s original planned timeline.

\subsection{Revised Project Abstract}

Many variables affect the quality of a cooked steak, which should finish within a narrow range of acceptable internal temperatures, and a good, even sear on each side.  If the cooking surface is too hot, the sides will burn instead of sear, and it becomes very easy to overshoot the desired internal temperature.  If the cooking surface is too cold, the sear will be imperfect, and the steak will lack the flavour contributed by the Maillard reaction.  If the steak is flipped too early or too late, the sides will not be seared evenly, and the cooking gradient through the steak will not be consistent.  To the amateur chef, the perfect steak is rare and elusive.  The objective of this project is to design and build a device capable of automatically cooking a steak to the user’s desired doneness on a consumer range or hotplate, tracking the internal and surface temperatures of the steak, flipping it when necessary, and removing it from the heat when cooking has finished.  The Copper Chef requires far less effort than manual steak-cooking methods, and is more convenient and affordable than existing automatic alternatives.

\subsection{Original Project Timeline}

Figure 1 shows the original planned timeline for the first term of development for The Copper Chef project.

\begin{figure}[h!]
\centering
\begin{ganttchart}[x unit=0.2cm, y unit chart=0.5cm, time slot format=isodate]{2017-06-1}{2017-07-28}

\gantttitle{498A}{58} \\

\gantttitlecalendar{month = name} \\

\ganttbar{Design first prototype}{2017-06-1}{2017-06-30} \\
\ganttbar{Design cooking algorithm}{2017-06-10}{2017-06-20} \\
\ganttbar{Implement software}{2017-06-21}{2017-07-12} \\
\ganttbar{Document design}{2017-06-20}{2017-06-30} \\
\ganttbar{Order parts}{2017-06-11}{2017-07-5} \\
\ganttbar{Build structural framework}{2017-07-6}{2017-07-10} \\
\ganttbar{Interface microcontroller}{2017-07-6}{2017-07-10} \\
\ganttbar{Implement power system}{2017-07-6}{2017-07-12} \\
\ganttbar{Test prototype}{2017-07-13}{2017-07-20} \\
\ganttbar{Demonstrate prototype}{2017-07-21}{2017-07-21} \\
\ganttbar{Revise abstract \& timeline}{2017-07-22}{2017-07-28} \\
\ganttbar{Refine design}{2017-07-22}{2017-07-28}

\end{ganttchart}
\caption{498A timeline}
\end{figure}

Figure 2 shows the original planned timeline for the first term of development for The Copper Chef project.

\begin{figure}[h!]
\centering
\begin{ganttchart}[x unit=0.15cm, y unit chart=0.5cm, time slot format=isodate]{2018-01-3}{2018-03-14}

\gantttitle{498B}{71} \\

\gantttitlecalendar{month = name} \\

\ganttbar{Refine first prototype}{2018-01-3}{2018-01-26} \\
\ganttbar{Implement doneness selection}{2018-01-3}{2018-01-26} \\
\ganttbar{Create final report}{2018-01-27}{2018-03-10} \\
\ganttbar{Improve aesthetic design}{2018-01-27}{2018-02-12} \\
\ganttbar{Perform final testing}{2018-02-13}{2018-02-20} \\
\ganttbar{Prepare for symposium}{2018-02-21}{2018-03-13} \\
\ganttbar{Demonstrate final prototype}{2018-03-12}{2018-03-12} \\
\ganttbar{Setup for symposium}{2018-03-13}{2018-03-13} \\
\ganttbar{Demonstrate at symposium}{2018-03-14}{2018-03-14}

\end{ganttchart}
\caption{498B timeline}
\end{figure}

\section{Current Status of Project}

This section describes the status of The Copper Chef project after its first four months of progress.

\subsection{Prototype Completion}

The Copper Chef is structurally complete, with a finished frame that supports the vertical motion drive.  Two stepper motors have been attached to the frame, and are connected to two threaded rods.  When rotated, the rods screw themselves into threads in the rotating arm, resulting in linear vertical motion.  The Copper Chef’s Infrared (IR) temperature sensor has been successfully interfaced with the purchased Arduino microcontroller.  Basic software has been written to raise, lower and rotate the arm, and receive data from the thermometer.  At the end of the first term of development, The Copper Chef has a completed feedback loop, with the ability to actuate change and sense the result of that change.

The Copper Chef’s rotational arm---the component that will hold the steak---has not yet been constructed.  While a hypothetical cooking algorithm has been proposed, it exists only in pseudocode, and has not yet been validated through practical trials.

The core functional feedback loop is complete, and the second term of development will focus on the completion of the mechanical design, polish, and precise manipulation of the algorithm.  Constructing the rotational steak-holding arm will be the first priority, followed by a general cleanup of wiring and electrical implementation.  After these tasks are complete, The Copper chef will be mechanically complete, and focus will shift to the perfection of the cooking algorithm.  It is estimated that The Copper Chef’s prototype is 65 \% complete based on what is done and what remains to be done, which is consistent with our advisor Professor Christopher Nielsen’s estimate.

\subsection{Student Hours}

Our hours are lower than the recommended 120 hours per student.  We believe that we were able to work more efficiently than is expected of fourth-year design project groups due to an ideal choice of components, and a good use of resources and expert guidance available to University of Waterloo undergraduate engineering students.  The Arduino ecosystem that we chose to work in is renowned for ease of implementation and availability of resources.  Code samples and external libraries exist demonstrating everything needed for the operation of The Copper Chef, allowing us to easily implement the motor controls, and the IR sensor.

\begin{table}[h!]
\centering
\begin{tabular}{| l | r |}
	\hline
	Eric Bender & 74.75 \\
	Shiva Ehtezazi & 72.75 \\
	Taylor Laekeman & 91.5 \\
	Christian Petri & 91 \\
	Brian Truong & 88.5 \\ \hline
	Total & 418.5 \\
	\hline
\end{tabular}
\caption{Student hours}
\end{table}

A large amount of time was spent considering and iterating on the mechanical design of The Copper Chef.  We frequently discussed our designs with experts, such as Professor Christopher Nielsen, our advisor, and the professionals in the Student Machine Shop.  The guidance we received helped us to construct a working prototype much more quickly and proficiently than we otherwise would have been able to.

\section{Discussion}

It can be said with confidence that the project will be 100 \% complete by the March symposium.  At the end of the first term of development, The Copper Chef is more than 50 \% complete, and is ahead of the implementation schedule we set for it.  The mechanical subsection of the device, which was thought to be the most challenging part of the project, is essentially complete, with the motors attached and the frame built.  The sensor interface has also been implemented and the temperature data is correctly propagating back to the microprocessor.  The next term of development will focus largely on polish and the fine manipulation of the cooking algorithm, before moving on the non-essential specifications.  Given the estimate that the project is currently 65 \% complete, and the fine tuning will be the focus of the next term, it can be said with confidence that this project will be 100 \% complete by the March symposium. 

The group’s consultant, Professor Christopher Nielsen, confirms that the project is 50 \% completed and that the project is challenging enough for a fourth year design project.

\appendix

\newpage
\pagenumbering{gobble}
\section{Student Logs}

\newpage
\section{Initial Prototype Demonstration Feedback Sheet}

\end{document}
